import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UserRepository } from './user.repository';

@Module({
  controllers: [AuthController],
  imports: [TypeOrmModule.forFeature([UserRepository])],
  providers: [AuthService],
})
export class AuthModule {}
