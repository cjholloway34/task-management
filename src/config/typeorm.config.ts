import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { User } from '../auth/user.entity';
import { Task } from '../tasks/task.entity';

export const typeOrmConfig: TypeOrmModuleOptions = {
  database: 'taskmanagement',
  entities: [Task, User],
  host: 'localhost',
  password: '**nimda**',
  port: 5432,
  type: 'postgres',
  synchronize: true,
  username: 'postgres',
};
